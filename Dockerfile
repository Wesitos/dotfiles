ARG USER=wesitos
ARG IMAGE=ubuntu:24.04

FROM ${IMAGE}
ARG USER
ENV USER=$USER

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /home/$USER/dotfiles

RUN useradd -m ${USER}
RUN touch /etc/modules
RUN apt-get update && \
    apt-get install -y lsb-release sudo fake-hwclock curl

RUN ln -s /usr/sbin/fake-hwclock  /usr/sbin/hwclock

# Add docker-systemctl-replacement
RUN curl -LsSf \
    https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/refs/heads/master/files/docker/systemctl3.py > /usr/bin/systemctl \
    && chmod 755 /usr/bin/systemctl

COPY --chown=$USER . .

ENV PROVISION_HOSTNAME=lorentz

# RUN ./provision/run.sh -v -e dind_test=true --skip-tags hardware

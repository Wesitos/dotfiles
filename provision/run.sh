#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function distro_id () {
    lsb_release -is 2> /dev/null | awk '{print tolower($0)}'
}

function ubuntu_codename() {
    if is_debian; then
        DEBIAN_RELEASE=$(lsb_release -rs 2> /dev/null)

        case $DEBIAN_RELEASE in
            10) echo "bionic" ;;
            11) echo "focal" ;;
            12 | n/a) echo "jammy" ;;
        esac
        return
    fi

    lsb_release -cs 2> /dev/null | awk '{print tolower($0)}'
}

function distro_release () {
    lsb_release -rs 2> /dev/null
}

function is_ubuntu () {
    [ $(distro_id) == "ubuntu" ] || [ $(distro_id) == "pop" ]
}

function is_debian () {
    [ $(distro_id) == "debian" ] || [ $(distro_id) == "raspbian" ]
}

function add_src_deb822 () {
    if [ -f ${1} ]; then
        echo "Adding deb-src sources to ${1}"
        ${SUDO} sed -i '/^Types: deb$/ s/$/ deb-src/g' ${1}
    fi
}

function log () {
    case $1 in
        "-s")
            echo
            echo "${@:2}"
            ;;
        *)
            echo "> ${@}"
            ;;
    esac
}

SUDO=""
SUDO_NO_ENV=""

if [ "$EUID" -eq 0 ]; then
    log "Ran as root"
else
    log 'Ran as non-root user'
    SUDO="sudo -E -u root --"
    SUDO_NO_ENV="sudo -u root --"
fi

apt_dir=/etc/apt
sources_dir=${apt_dir}/sources.list.d

apt_deps=(
    "software-properties-common" \
    "curl"
)

if ! command -v lsb_release > /dev/null; then
    log "Installing lsb_release"
    ${SUDO} apt-get install -y lsb-release
fi

log -s "Configuring apt sources"

if [ -f ${sources_dir}/debian.sources ]; then
    log "Found 'debian.sources' file"

    add_src_deb822 ${sources_dir}/debian.sources

elif [ -f ${sources_dir}/ubuntu.sources ]; then
    log "Found 'ubuntu.sources' file"

    add_src_deb822 ${sources_dir}/ubuntu.sources
else
    log "Falling back to using 'sources.list' file"

    log "Ensuring deb-src lines are uncommented"
    ${SUDO} sed -i '/deb-src/s/^# //g' ${apt_dir}/sources.list
fi


log -s "Ensuring ansible is installed"

if is_ubuntu || is_debian; then
    log "Detected Debian-like system"

    installed_pkgs=$(dpkg-query -f '${Package}\n' -W ${apt_deps[*]}; true)

    if test "$(echo ${installed_pkgs} | wc -w)" -ne ${#apt_deps[@]}; then
        log "Installing dependencies"
        ${SUDO} apt-get update
        ${SUDO} apt-get install -y "${apt_deps[*]}"
    fi
else
    log "Unsuported distro $(distro_id)"
    exit 1
fi

if ${SUDO} test ! -x /root/.local/bin/uv; then
    log "Installing uv"
    curl -LsSf https://astral.sh/uv/install.sh | ${SUDO_NO_ENV} env INSTALLER_NO_MODIFY_PATH=1 sh
fi

log "Ensuring ansible-core is updated"
${SUDO_NO_ENV} /root/.local/bin/uv tool install \
        --quiet --upgrade --force --python 3.13 ansible-core \
        --with python-debian \
        --with requests \
        --with docker

cd $DIR

export ANSIBLE_CONFIG="${DIR}/ansible.cfg"
export ANSIBLE_HOME="/tmp/.ansible"

mkdir -p "${ANSIBLE_HOME}"

${SUDO} /root/.local/bin/ansible-galaxy install -r "${DIR}/requirements.yml"
${SUDO} chown -R "$(stat -c '%u:%g' ${DIR})" "${ANSIBLE_HOME}"

log -s "Running playbook"
${SUDO} /root/.local/bin/ansible-playbook setup.yml \
        --connection=local \
        --inventory=host-features.yml "$@"
